## Gossip task division simulation
This program simulates a multi-agent task division problem with gossip.
### Running the program
Required python version: Developed for 3.7, but any version above 3 should work.

To run the program, execute: 

```python main.py```

### Adjusting the parameters
Adjust parameters by editing parameters.py

### Collaborators: 
* Jorn van Wier
* Ruurd Bijlsma
* Jorge Anton Garcia
* Alexandros Charisis

